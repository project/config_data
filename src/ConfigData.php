<?php

namespace Drupal\config_data;

use \Drupal\Core\State\State;

class ConfigData {

  public static function key($config_key, $data_key) {
    return "config_data/{$config_key}/{$data_key}";
  }

  public static function get($config_key, $data_key) {
    return State::get(self::key($config_key, $data_key));
  }

  public static function set($config_key, $data_key, $value) {
    State::set(self::key($config_key, $data_key), $value);
  }
}