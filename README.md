# Config Data
This module provides a Config Data API for storing and overriding 
configuration with data persisted in the State API. 

## What is Configuration Data?
When we do configuration in Drupal sites, we are generally 
providing two types of information.

1) Logic: Content types, fields, views, etc.
2) Data: Site name, GA tracker code, GTM code, api key, etc.

For example, when configuring the google analytics module,
there are several sections to specify tracked paths, roles,
dimensions, custom javascript, etc. This logic is essentially code. 
Then there is a field to specify your tracker id. Many,
but not all, would consider this data. 

## Why a separate API?
Drupal treats all configuration information the same.
If it's stored in the configuration system, it is meant 
to be exported, version controlled, and deployed between 
environments (dev, stage, prod).

There is an assumption that any configuration change that happens
should go through a workflow involving a developer or similarly
technical person. This creates problems for anyone who may wish
to delegate safe, minor changes to non-technical site owners.

If you have a distribution with hundreds of sites, or a single
site managed by a client, you may want to version control
99% of your configuration while allowing the other 1% to be 
controlled by the client.

## Using the API

